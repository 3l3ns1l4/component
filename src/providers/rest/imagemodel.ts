
export class ImageModel {
    format: string;
    width: number;
    height: number;
    filename: string;
    id: number;
    author: string;
    author_url: string;
    post_url: string;
 
    constructor(values: Object = {}) {
        Object.assign(this, values);
      
    }
}
