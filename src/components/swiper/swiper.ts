import { Component, ElementRef, ViewChild, NgZone, OnDestroy, AfterViewInit, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Swiper, Pagination, Scrollbar } from 'swiper/dist/js/swiper.esm.js';
import { RestProvider } from '../../providers/rest/rest';
import { ImageModel } from '../../providers/rest/imagemodel';
import { first } from 'rxjs/operators';
import { Subscription } from 'rxjs/Subscription';

//import { Slides } from 'ionic-angular';
//Swiper.use([Pagination]);
@Component({
  selector: 'comp-swiper',
  templateUrl: 'swiper.html'
})
export class SwiperComponent implements OnDestroy, AfterViewInit {

  @ViewChild('swiper') swiperHtml: ElementRef;
  @ViewChild('paginator') paginatorHtml: ElementRef;
  @Input() baseUrl: string;
  @Input() id: string;

  public swiper: Swiper;
  public counter: number = 0;
  public slides: Array<ImageModel>;
  public images: Array<ImageModel>;
  private dataSource: Subscription;
  public dim: any = {};
  constructor(
    public navCtrl: NavController,
    private imageProvider: RestProvider,
    public zone: NgZone
  ) { }

  addSlide(c: number): string {
    let el: ImageModel = this.images[c];
    let tpl: string = "https://picsum.photos/" + el.width + "/" + el.height + "?image=" + el.id;
    return '<div class="swiper-slide"><img  class="full-size" src="' + tpl + '" /></div>';
  }

  updateSwiper(c: number) {
    this.swiper.slides[c].querySelector('img').src = "";
    this.swiper.removeSlide(c);
    this.swiper.update();
  }

  getTriad(images:Array<ImageModel>):Array<ImageModel>{
    return  (images.length > 3)?   images.slice(0, 3) :images ;
  }

  getSlides() {

    this.dataSource = this.imageProvider.getImages(this.baseUrl+"?timeStamp="+this.id+"_"+Date.now())
      .subscribe(
        (images:Array<ImageModel>) => {
        
          this.imageProvider.shuffle(images);
          this.slides=this.getTriad(images);
          console.log("id:", this.id  ,this.slides.length ) ;
          this.images = images;
          /* this.zone.onStable.asObservable().pipe(first()).subscribe(() => {
            if (this.slides.length > 1) {
              this.setSlider();
            }

          }); */

          setTimeout(() => {
            if (this.slides.length > 1) {
              this.setSlider();
            }
          }, 250)


        },
        error => console.log("error")
      );
  }

  setSlider() {
    this.swiper = new Swiper(this.swiperHtml.nativeElement, {
      preloadImages:false,
      on: {
        //init:()=> console.log("init", this.id , this.slides  ),
        slideNextTransitionEnd: () => {
          if (this.swiper.slides.length == 2) {
            return;
          }
          this.counter++;
          if (this.counter >= 2) {
            //console.log("ADD");
            this.swiper.appendSlide(this.addSlide(this.counter + 1));
            this.updateSwiper(0);
          }
          // console.log('Current index is', this.swiper.activeIndex,  " - count:" ,this.counter);
        },
        slidePrevTransitionEnd: () => {
          if (this.swiper.slides.length == 2) {
            return;
          }
          if (this.swiper.activeIndex === 0 && this.counter >= 2) {
            this.swiper.prependSlide(this.addSlide(this.counter - 2))
            this.updateSwiper(2);
          }
          if (this.swiper.activeIndex === 1) {
            //console.log("CHANGE",this.swiper.slides[2]);
            this.swiper.appendSlide(this.addSlide(this.counter));
            this.updateSwiper(2);
          }
          this.counter--;
          // console.log('Current index is', this.swiper.activeIndex,  " - count:" ,this.counter);
        }
      }


    });

    if(this.slides.length>0 && this.swiper.slides.length===0 ){
      console.log("init", this.id , this.slides ,this.swiper.slides.length );
      this.zone.run(() => { 
        this.slides=this.getTriad(this.images);
        setTimeout(() => {
          this.swiper.updateSlides();
        }, 250)       
      });
    
    }

   

  }

  ngOnInit() {
    let bound: ClientRect = this.swiperHtml.nativeElement.getBoundingClientRect();
    this.dim.w = bound.width;
    this.dim.h = bound.height;
  }

  ngAfterViewInit(): void {
    this.getSlides();
  }
  ngOnDestroy(): void {
    if (this.dataSource) {
      this.dataSource.unsubscribe();
    }
    if (this.swiper) {
    

      this.swiper.detachEvents();
      this.swiper.removeSlide(this.swiper.slides);
      this.swiper.destroy(true, true);
    }
  }




}

