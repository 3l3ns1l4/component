import { NgModule } from '@angular/core';
import { SwiperComponent } from './swiper/swiper';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import {CommonModule} from '@angular/common';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [SwiperComponent],
	imports: [
		BrowserModule,
		HttpClientModule,
		CommonModule,
		IonicModule
	],
	exports: [SwiperComponent]
})
export class ComponentsModule {}
